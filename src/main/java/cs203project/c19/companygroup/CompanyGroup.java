
package cs203project.c19.companygroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cs203project.c19.user.User;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
// @NoArgsConstructor
@EqualsAndHashCode
public class CompanyGroup implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Company Group name should not be null")
    @Size(min = 3, max = 30, message = "Company Group name should be between 5 and 20 characters")
    private String companyGroupName;

    @NotNull(message = "Industry should not be null")
    private String companyGroupIndustry;

    // @NotNull(message = "Owner cannot be null")
    private String companyGroupOwner;

    @OneToMany(mappedBy = "companyGroup", cascade = CascadeType.PERSIST, orphanRemoval = false)
    @JsonIgnore
    private List<User> users = new ArrayList<User>();

    public CompanyGroup(String companyGroupName, String companyGroupIndustry, String companyGroupOwner) {
        this.companyGroupName = companyGroupName;
        this.companyGroupIndustry = companyGroupIndustry;
        this.companyGroupOwner = companyGroupOwner;
    }

    public CompanyGroup() {
    }

    public void setCompanyGroupName(String companyGroupName) {
        this.companyGroupName = companyGroupName;
    }

    public String getCompanyGroupName() {
        return this.companyGroupName;
    }

    public void setCompanyGroupIndustry(String companyGroupIndustry) {
        this.companyGroupIndustry = companyGroupIndustry;
    }

    public String getCompanyGroupIndustry() {
        return this.companyGroupIndustry;
    }

    public List<User> getUsersCompanyGroup() {
        return users;
    }

    public void setUsersCompanyGroup(List<User> users) {
        this.users = users;
    }

    public String getCompanyGroupOwner(CompanyGroup companyGroup) {
        return this.companyGroupOwner;
    }

    public void setCompanyGroupOwner(String companyGroupOwner) {
        this.companyGroupOwner = companyGroupOwner;
    }

}
