package cs203project.c19.companygroup;

import java.util.List;
import cs203project.c19.user.User;

public interface CompanyGroupService {
    List<CompanyGroup> listCompanyGroups();
    CompanyGroup getCompanyGroup(Long id);
    CompanyGroup addCompanyGroup(CompanyGroup companyGroup);
    CompanyGroup updateCompanyGroup(Long id, CompanyGroup companyGroup);
    void addUserCompanyGroup(User user, CompanyGroup companyGroup);
    void addCompanyGroupOwner(User user, CompanyGroup companyGroup);

    /**
     * Change method's signature: do not return a value for delete operation
     * @param id
     */
    void deleteCompanyGroup(Long id);
}



