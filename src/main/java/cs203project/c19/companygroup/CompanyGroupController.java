package cs203project.c19.companygroup;

import java.util.List;

import javax.mail.MessagingException;

import java.io.UnsupportedEncodingException;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

import cs203project.c19.user.User;
import cs203project.c19.user.UserRepository;

@RestController
public class CompanyGroupController {
    private CompanyGroupRepository companyGroupRepo;
    private UserRepository userRepo;
    private CompanyGroupServiceImpl companyGroupService;


    @Autowired
    public CompanyGroupController(CompanyGroupRepository companyGroupRepo, UserRepository userRepo, CompanyGroupServiceImpl companyGroupService) {
        this.companyGroupRepo = companyGroupRepo;
        this.userRepo = userRepo;
        this.companyGroupService = companyGroupService;
    }
    
    //temp json page
    // @RequestMapping("/groups")
    // @ResponseBody
    // public List<CompanyGroup> getCompanyGroups(){
    //     return companyGroupService.listCompanyGroups();
    // }

        /**
     * allows user to view the groups page
     * 
     * @param user check whether the user is an employee or employer
     * @return returns the html file for the groups webpage
     */
    @RequestMapping("/groups")
    public ModelAndView viewGroupsPage(/*Model model,*/ CompanyGroup companyGroup) {
        // model.addAttribute("companygroup", new CompanyGroup());
        ModelAndView modelAndView = new ModelAndView();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        if (user.getEmploymentType().contains("Employer") && user.getCompanyGroup() != null) { // employer with group
            System.out.println("view employer with group");
            modelAndView.setViewName("employer_group_page_created");
            modelAndView.addObject("usercompanyGroups", companyGroupService.getUsersCompanyGroup(user.getCompanyGroup()));
            modelAndView.addObject("userCompanygroupName", user.getCompanyGroup().getCompanyGroupName());
            return modelAndView;
        } else if (user.getEmploymentType().contains("Employee") && user.getCompanyGroup() == null) { // employee no group
            System.out.println("view employee no group");
            modelAndView.setViewName("employee_group_page");
            modelAndView.addObject("companyGroups", companyGroupService.listCompanyGroups());
            return modelAndView;
        } else if (user.getEmploymentType().contains("Employer") && user.getCompanyGroup() == null) { // employer no group
            System.out.println("view employer no group");
            modelAndView.setViewName("employer_group_page");
            return modelAndView;
        } else { // employee with group
            modelAndView.addObject("companyGroupOwner", user.getCompanyGroup().getCompanyGroupOwner());
            System.out.println("view employee with group");
            modelAndView.addObject("userVaccinationStatus", user.getIsVaccinated());
            modelAndView.addObject("userCompanygroupName", user.getCompanyGroup().getCompanyGroupName());
            modelAndView.setViewName("employee_group_page_joined");
            return modelAndView;
        }
    }

    @RequestMapping("/create_group")
    public ModelAndView viewCreateGroupPage(Model model) {
        model.addAttribute("companyGroup", new CompanyGroup());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("create_group_form");
        return modelAndView;
    }
    
    @RequestMapping("/process_create_group")
    public ModelAndView processCreateGroup(CompanyGroup companyGroup/*, HttpServletRequest request*/)
             throws UnsupportedEncodingException, MessagingException {
        CompanyGroup searchGroup = companyGroupRepo.findCompanyGroupBycompanyGroupName(companyGroup.getCompanyGroupName());
        System.out.println(searchGroup);
        ModelAndView modelAndView = new ModelAndView();
        if (searchGroup == null) {
            companyGroupService.addCompanyGroup(companyGroup);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String email = ((User) principal).getEmail();
            User user = userRepo.findByEmail(email);
            companyGroupService.addUserCompanyGroup(user, companyGroup);
            companyGroupService.addCompanyGroupOwner(user, companyGroup);
            userRepo.save(user);
            companyGroupRepo.save(companyGroup);
            modelAndView.setViewName("create_group_success");
            return modelAndView;
        } else {
            modelAndView.setViewName("create_group_fail");
            return modelAndView;
        }
    }

    @RequestMapping("/delete_group")
    public ModelAndView processDeleteGroup(CompanyGroup companyGroup) {
        ModelAndView modelAndView = new ModelAndView();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        if (user.getEmploymentType().contains("Employer") && user.getCompanyGroup() != null 
            /*&& user.getUsername().contains(companyGroupService.getCompanyGroupOwner(companyGroup))*/) {
            System.out.println(user.getUserCompanyGroup().getCompanyGroupName());
            String companyName = user.getUserCompanyGroup().getCompanyGroupName();
            List<User> listofusersincompanygroup = user.getUserCompanyGroup().getUsersCompanyGroup();
            for (int i = 0 ; i < listofusersincompanygroup.size() ; i++) {
                listofusersincompanygroup.get(i).setUserCompanyGroup(null);
                userRepo.save(listofusersincompanygroup.get(i));
            }
            companyGroupRepo.delete(companyGroupRepo.findCompanyGroupBycompanyGroupName(companyName));
            return viewGroupsPage(companyGroup);
        }
        modelAndView.setViewName("employer_group_page_created");
        return modelAndView;
    }

    @PostMapping("/join_group")
    public ModelAndView processJoinGroup(CompanyGroup companyGroup) {
        System.out.println("see below");
        System.out.println("see above");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        CompanyGroup grp = companyGroupRepo.findCompanyGroupBycompanyGroupName(companyGroup.getCompanyGroupName());
        System.out.println(grp.getCompanyGroupName());
        companyGroupService.addUserCompanyGroup(user, grp);
        userRepo.save(user);
        companyGroupRepo.save(grp);
        return viewGroupsPage(companyGroup);

    }

    @RequestMapping("/leave_group")
    public ModelAndView processLeaveGroup(CompanyGroup companyGroup) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        System.out.println(user.getUsername());

        user.setUserCompanyGroup(null);
        userRepo.save(user);
        return viewGroupsPage(companyGroup);
    }
}
