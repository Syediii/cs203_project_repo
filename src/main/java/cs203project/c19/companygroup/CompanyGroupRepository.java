package cs203project.c19.companygroup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyGroupRepository extends JpaRepository<CompanyGroup, Long>{

     //Optional<CompanyGroup> findByName(String companyGroupName);
    // @Query("SELECT grp FROM COMPANY_GROUP grp WHERE grp.COMPANY_GROUP_NAME = companyGroupName")
    // CompanyGroup findByName(String companyGroupName);

    // public CompanyGroup findByName(String companyGroupName);
    CompanyGroup findCompanyGroupBycompanyGroupName(String companyGroupName);

    // @Modifying
    // @Query(UPDATE User SET COMPANY_GROUP_ID = 'Alfred Schmidt' WHERE ID = 1)
    // void updateUserCompanyGroup()
    Long deleteBycompanyGroupName(String companyGroupName);

    // Long deleteBycompanyGroupId(Long companyGroupId);
}
