package cs203project.c19.companygroup;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import cs203project.c19.user.UserRepository;
import cs203project.c19.exception.CompanyGroupNotFoundException;
import cs203project.c19.user.User;
@Service
public class CompanyGroupServiceImpl implements CompanyGroupService {

    private CompanyGroupRepository companyGroups;
    private UserRepository userRepo;

    @Autowired
    public CompanyGroupServiceImpl(CompanyGroupRepository companyGroups, UserRepository userRepo) {
        this.companyGroups = companyGroups;
        this.userRepo = userRepo;
    }

    @Override
    public List<CompanyGroup> listCompanyGroups() {
        return companyGroups.findAll();
    }

    @Override
    public CompanyGroup getCompanyGroup(Long id) {
        return companyGroups.findById(id).map(companyGroup -> {
            return companyGroup;
        }).orElse(null);
    }

    public CompanyGroup addCompanyGroup(CompanyGroup companyGroup) {
        return companyGroups.save(companyGroup);
    }

    @Override
    public CompanyGroup updateCompanyGroup(Long id, CompanyGroup newGroupInfo) {
        return companyGroups.findById(id).map(companyGroup -> {companyGroup.setCompanyGroupName(newGroupInfo.getCompanyGroupName());
            companyGroup.setCompanyGroupIndustry(newGroupInfo.getCompanyGroupIndustry());
            return companyGroups.save(companyGroup);
        }).orElse(null);
    }

    @Override
    public void deleteCompanyGroup(Long id){
        companyGroups.deleteById(id);
    }

    public void addUserCompanyGroup(User user, CompanyGroup companyGroup) {
        user.setCompanyGroup(companyGroup);
    }

    public void addCompanyGroupOwner(User user, CompanyGroup companyGroup) {
        companyGroup.setCompanyGroupOwner(user.getUsername());
    }

    public String getCompanyGroupOwner(CompanyGroup companyGroup) {
        return companyGroup.getCompanyGroupOwner();
    }

    public CompanyGroup get(long id) throws CompanyGroupNotFoundException{
        Optional<CompanyGroup> result = companyGroups.findById(id);
        if (result.isPresent()){
            return result.get();
        }
        throw new CompanyGroupNotFoundException("Could not find any Company Group with the id " + id);
    }

    public List<User> getUsersCompanyGroup(CompanyGroup companyGroup) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        CompanyGroup grp = user.getCompanyGroup();
        // grp.getUsersCompanyGroup();
        List<User> usersInGroup = grp.getUsersCompanyGroup();
        return usersInGroup;

    }

}
