package cs203project.c19.user;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cs203project.c19.content.FileController;
import cs203project.c19.content.FileServiceImpl;
import cs203project.c19.exception.UserNotFoundException;
import net.bytebuddy.utility.RandomString;

import org.springframework.ui.Model;

@RestController
public class UserController {
    @Autowired
    private CustomUserDetailsService userService;

    private UserRepository userRepo;
    private BCryptPasswordEncoder encoder;
    private FileServiceImpl fileService;


    @Autowired
    public UserController(UserRepository userRepo, BCryptPasswordEncoder encoder, FileServiceImpl fileService) {
        this.userRepo = userRepo;
        this.encoder = encoder;
        this.fileService = fileService;
    }

    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping("/adminDashboard")
    @ResponseBody
    private List<User> adminDashboard(Model model) {
        return userRepo.findAll();
    }

    @RequestMapping("/userDashboard")
    public ModelAndView userDashboard() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        modelAndView.addObject("newsPosts", fileService.getFiles());
        return modelAndView;
    }

    /**
     * allows user to view the register page
     * 
     * @param model has been designed for adding attributes to the model
     * @return returns the html file for the register webpage
     */
    @RequestMapping("/register")
    public ModelAndView viewRegisterPage(Model model) {
        model.addAttribute("user", new User());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register_form");
        return modelAndView;
    }

    /**
     * allows user to view either the next page (succesful registration or failed
     * registration) after they submit their registration form. this function also
     * adds the user's details to the database.
     * 
     * @param user    is a user object
     * @param request contains client's request information
     * @return returns the html file for the corresponding webpage
     */

    @RequestMapping("/process_register")
    public ModelAndView processRegister(User user, HttpServletRequest request)
            throws UnsupportedEncodingException, MessagingException {
        User searchUser = userRepo.findByEmail(user.getEmail());
        System.out.println(searchUser);
        ModelAndView modelAndView = new ModelAndView();
        if (searchUser == null) {
            user.setAuthorities("ROLE_USER");
            userService.register(user, getSiteURL(request));
            modelAndView.setViewName("register_success");
            return modelAndView;
        } else {
            modelAndView.setViewName("register_fail");
            return modelAndView;
        }
    }

    private String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    /**
     * allows user to view the succesful verification page or failed verification
     * page
     * 
     * @param code contains the verification code that is needed to verify the
     *             user's account
     * @return returns the html file for the corresponding webpage
     */
    @GetMapping("/verify")
    public ModelAndView verifyUser(@Param("code") String code) {
        ModelAndView modelAndView = new ModelAndView();

        if (userService.verify(code)) {
            modelAndView.setViewName("verify_success");
            return modelAndView;
        } else {
            modelAndView.setViewName("verify_fail");
            return modelAndView;
        }
    }

    /**
     * Using BCrypt encoder to encrypt the password for storage
     * 
     * @param user
     * @return
     */
    @PostMapping("/users")
    public User addUser(@Valid @RequestBody User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepo.save(user);
    }

    // open settings handler
    @RequestMapping("/settings")
    public ModelAndView settings() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("settings");
        return modelAndView;
    }

    // change password handler
    @GetMapping("/change_password")
    public ModelAndView showChangePasswordForm(Model model) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("change_password");
        return modelAndView;
    }

    /**
     * allows user to view the succesful verification page or failed verification
     * page
     * 
     * @param model   has been designed for adding attributes to the model
     * @param request contains client's request information
     * @return returns the html file for the corresponding webpage
     */
    @PostMapping("/change_password")
    public ModelAndView processChangePassword(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = new ModelAndView();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();

        String password = request.getParameter("password");

        model.addAttribute("title", "Change your password");

        User user = userRepo.findByEmail(email);
        userService.updatePassword(user, password);

        model.addAttribute("message", "You have successfully changed your password.");

        modelAndView.setViewName("message");
        return modelAndView;
    }

    /**
     * allows user to view the forgot password page
     * 
     * @return returns the html file for the forgot password webpage
     */
    @GetMapping("/forgot_password")
    public ModelAndView showForgotPasswordForm() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forgot_password_form");
        return modelAndView;
    }

    @PostMapping("/forgot_password")
    public ModelAndView processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = getSiteURL(request) + "/reset_password?token=" + token;
            userService.sendResetPasswordEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (UserNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forgot_password_form");
        return modelAndView;
    }

    /**
     * allows user to view the succesful reset password page or failed reset
     * password page
     * 
     * @param model has been designed for adding attributes to the model
     * @param token is the password reset token that has been sent to the user's
     *              email
     * @return returns the html file for the corresponding webpage
     */
    @GetMapping("/reset_password")
    public ModelAndView showResetPasswordForm(@Param(value = "token") String token, Model model) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("token", token);

        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            modelAndView.setViewName("message");
            return modelAndView;
        }

        modelAndView.setViewName("reset_password_form");
        return modelAndView;
    }

    /**
     * allows user to view the succesful verification page or failed verification
     * page
     * 
     * @param model   has been designed for adding attributes to the model
     * @param request contains client's request information
     * @return returns the html file for the corresponding webpage
     */
    @PostMapping("/reset_password")
    public ModelAndView processResetPassword(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        String token = request.getParameter("token");
        String password = request.getParameter("password");

        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            modelAndView.setViewName("message");
            return modelAndView;
        } else {
            userService.updatePassword(user, password);

            model.addAttribute("message", "You have successfully changed your password.");
        }

        modelAndView.setViewName("message");
        return modelAndView;
    }

    @GetMapping("/change_details")
    public ModelAndView showChangeDetailsForm(HttpServletRequest request, Model model, RedirectAttributes ra) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String email = ((User) principal).getEmail();
            User user = userRepo.findByEmail(email);
            model.addAttribute("user", user);
            
        } catch (UserNotFoundException e) {
            ra.addFlashAttribute("message", "The user has been save successfully");
            modelAndView.setViewName("redirect:/settings");
            return modelAndView;
        }
        return modelAndView;

    }

    @PostMapping("/change_details")
    public ModelAndView processChangeDetails(HttpServletRequest request, Model model) {
        ModelAndView modelAndView = new ModelAndView();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((User) principal).getEmail();
        User user = userRepo.findByEmail(email);
        String workplace = request.getParameter("workplace");
        String isVaccinated = request.getParameter("isVaccinated");
        System.out.println(isVaccinated);


        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            modelAndView.setViewName("change_message");
            return modelAndView;
        } else {
            userService.updateWorkplace(user, workplace);
            if (isVaccinated.contains("Yes")){
                userService.updateIsVaccinated(user, true);
            } else {
                userService.updateIsVaccinated(user, false);
            }
            model.addAttribute("message", "You have successfully changed your details.");
        }

        modelAndView.setViewName("change_message");
        return modelAndView;
    }

}