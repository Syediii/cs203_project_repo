package cs203project.c19.user;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    // define a derived query to find user by username
    Optional<User> findByUsername(String username);

    User findByEmail(String email);

     /**
     * used to find a user using their verification code
     * @param code contains the verification code that is needed to verify the user's account
     */
    //@Query("SELECT u FROM User u WHERE u.verificationCode = ?1")
    public User findByVerificationCode(String code);
    
    /**
     * used to validate the token when the user clicks the change password link
     * @param token is the password reset token that has been sent to the user's email
     */
    public User findByResetPasswordToken(String token);

    Optional<User> findById(Long id);

    // List<User> findByCompanyGroupId(Long companyGroupId);

}
