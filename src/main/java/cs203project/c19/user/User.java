package cs203project.c19.user;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import cs203project.c19.companygroup.CompanyGroup;

import lombok.*;

/* 
Implementations of UserDetails to provide user information to Spring Security, 
e.g., what authorities (roles) are granted to the user and whether the account is enabled or not
*/

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
// @NoArgsConstructor
@EqualsAndHashCode
public class User implements UserDetails {
    // private @Id @GeneratedValue(strategy = GenerationType.AUTO) Long id;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Username should not be null")
    @Size(min = 5, max = 20, message = "Username should be between 5 and 20 characters")
    private String username;

    @NotNull(message = "Password should not be null")
    @Size(min = 8, message = "Password should be at least 8 characters")
    private String password;
    //private String confirmPassword;

    @NotNull(message = "Authorities should not be null")
    // We define two roles/authorities: ROLE_USER or ROLE_ADMIN
    private String authorities;

    @Email
    @NotNull(message = "Email should not be null")
    private String email;

    @NotNull(message = "Workplace should not be null")
    private String workplace;

    @Nullable
    private String verificationCode;

    @Nullable
    private String resetPasswordToken;

    @NotNull(message = "Enabled status should not be null")
    private boolean enabled;

    @NotNull(message = "Employment type should not be null")
    private String employmentType;

    @NotNull
    private Boolean isVaccinated;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "companyGroup_id", nullable = true)
    private CompanyGroup companyGroup;

    // public User(){
    //     super();
    //  }     

    public User() {}

    public User(String username, String password, String authorities, String email, 
                String workplace, String verificationCode, String resetPasswordToken, 
                Boolean enabled, String employmentType, Boolean isVaccinated) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.email = email;
        this.workplace = workplace;
        this.verificationCode = verificationCode;
        this.resetPasswordToken = resetPasswordToken;
        this.enabled = enabled;
        this.employmentType = employmentType;
        this.isVaccinated = isVaccinated;
    }

    // Return a collection of authorities granted to the user.
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority(authorities));
    }

    /*
     * The various is___Expired() methods return a boolean to indicate whether or
     * not the user’s account is enabled or expired.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
        //checkPassword();//check
    }

    // public void setConfirmPassword(String confirmPassword) {
    //     this.confirmPassword = confirmPassword;
    //     checkPassword();//check
    // }
    
    // private void checkPassword() {
    //     if (this.password == null || this.confirmPassword == null) {
    //         return;
    //     } else if (!this.password.equals(confirmPassword)) {
    //         this.confirmPassword = null;
    //     }
    // }


    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public void setIndustry(String workplace) {
        this.workplace = workplace;
    }

    public void getIndustry(String workplace) {
        this.workplace = workplace;
    }

    public String getVerificationCode() {
        return this.verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getResetPasswordToken() {
        return this.resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public UserDetails orElseThrow(Object object) {
        return null;
    }
    
    public String getEmploymentType() {
        return this.employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public Boolean getIsVaccinated() {
        return this.isVaccinated;
    }

    public void setIsVaccinated(Boolean isVaccinated) {
        this.isVaccinated = isVaccinated;
    }

    public CompanyGroup getUserCompanyGroup() {
        return this.companyGroup;
    }

    public void setUserCompanyGroup(CompanyGroup companyGroup) {
        this.companyGroup = companyGroup;
    }
}
