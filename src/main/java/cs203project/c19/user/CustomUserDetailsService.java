package cs203project.c19.user;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cs203project.c19.exception.UserNotFoundException;
import net.bytebuddy.utility.RandomString;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepo;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    
    @Autowired
    private JavaMailSender mailSender;

    /*
    To return a UserDetails for Spring Security 
    Note that the method takes only a username.
    The UserDetails interface has methods to get the password.
    */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("User '" + username + "' not found"));
    }

    public void register(User user, String siteURL) throws UnsupportedEncodingException, MessagingException {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);
        user.setEnabled(false);

        userRepo.save(user);

        sendVerificationEmail(user, siteURL);
    }

        /**
     * sends the verification email to the user's email after they have registered
     * @param user is the user object of the user who registered in the website
     * @param siteURL is the real context path of the web application
     */
    private void sendVerificationEmail(User user, String siteURL)
            throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "noreply.c19@gmail.com";
        String subject = "Verify your registration on C19";
        String senderName = "C19 Team";
        String content = "Dear [[name]],<br>" + "Please click the link below to verify your registration:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>" + "Thank you,<br>" + "C19 Team";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", user.getUsername());
        String verifyURL = siteURL + "/verify?code=" + user.getVerificationCode();

        content = content.replace("[[URL]]", verifyURL);

        helper.setText(content, true);

        mailSender.send(message);
    }

    /**
     * getters and setters for the variables of the user table
     * @verificationCode contains the code that is needed to enable the user's account
     * @return false if there is no such user or the account has already been enables
     * true if the user exists and the account has been verified normally
     */
    public boolean verify(String verificationCode) {
        User user = userRepo.findByVerificationCode(verificationCode);
         
        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setVerificationCode(null);
            user.setEnabled(true);
            userRepo.save(user);
             
            return true;
        }
         
    }

    /**
     * updates the randomly generated reset password token in the database
     * the function will throw a UserNotFoundException when user is not registered in the database
     * @token contains the reset password token
     * @email is the email of the user who requested to reset their password
     */
    public void updateResetPasswordToken(String token, String email) throws UserNotFoundException {
        User user = userRepo.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepo.save(user);
        } else {
            throw new UserNotFoundException("Could not find any user with the email " + email);
        }
    }
    
    /**
     * retrieves the randomly generated reset password token
     * @token contains the reset password token
     */
    public User getByResetPasswordToken(String token) {
        return userRepo.findByResetPasswordToken(token);
    }
    
    /**
     * updates the user's old password with the new password in the database
     * @user is the user object of user who requested to reset their password
     * @newPassword is the new password that has been sent by the user
     */
    public void updatePassword(User user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);
         
        user.setResetPasswordToken(null);
        userRepo.save(user);
    }

    public void updateWorkplace(User user, String newWorkplace) {
        user.setWorkplace(newWorkplace);
        userRepo.save(user);
    }

    public void updateIsVaccinated(User user, Boolean newIsVaccinated) {
        user.setIsVaccinated(newIsVaccinated);
        userRepo.save(user);
    }

    public void updateIsVaccinatedTest(User user, Boolean newIsVaccinated) {
        user.setIsVaccinated(newIsVaccinated);
    }


    /**
     * sends the password reset email to the user
     * @recipientEmail is the email of the user who requested to reset their password
     * @link is the link which the user needs to access in order to reset their password
     */
    public void sendResetPasswordEmail(String recipientEmail, String link)
    throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("noreply.c19@gmail.com", "C19 Team");
        helper.setTo(recipientEmail);

        String subject = "Password reset for C19";

        String content = "<p>Hello,</p>" +
            "<p>You have requested to reset your password.</p>" +
            "<p>Please click the link below to change your password:</p>" +
            "<p><a href=\"" + link + "\">Change my password</a></p>" +
            "<br>" +
            "<p>Ignore this email if you do remember your password, " +
            "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }

    public User get (long id) throws UserNotFoundException{
        Optional <User> result = userRepo.findById(id);
        if (result.isPresent()){
            return result.get();
        }
        throw new UserNotFoundException("Could not find any user with the id " + id);
    }
}
