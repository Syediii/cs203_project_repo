package cs203project.c19.exception;

public class ContentNotFoundException extends RuntimeException{
    //exception thrown when content is not found
    public ContentNotFoundException(Long id) {
        super("Could not find content " + id);
    }
}
