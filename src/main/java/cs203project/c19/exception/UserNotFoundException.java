package cs203project.c19.exception;

public class UserNotFoundException extends RuntimeException{

    /**
     * exception thrown when user is not found in the databse
     */
    public UserNotFoundException (String message){
        super(message);
    }
}