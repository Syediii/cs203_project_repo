package cs203project.c19.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CompanyGroupNotFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 1L;

    public CompanyGroupNotFoundException(Long id) {
        super("Could not find Company Group " + id);
    }

    public CompanyGroupNotFoundException (String message){
        super(message);
    }
}
