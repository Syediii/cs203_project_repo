package cs203project.c19.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	AuthenticationSuccessHandler successHandler;


	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
		http.headers().frameOptions().sameOrigin();

		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/userDashboard").hasAnyRole("USER")
			.antMatchers("/adminDashboard").hasAnyRole("ADMIN")
			//.antMatchers("/h2-console/**").hasRole("ADMIN")
			.and().formLogin().loginPage("/")
				.successHandler(successHandler)
			.and().logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))            
			.logoutSuccessUrl("/")
			.invalidateHttpSession(true)        // set invalidation state when logout
			.deleteCookies("JSESSIONID");
    }
	
    /*
    @Bean annotation is used to declare a PasswordEncoder bean in the Spring application context. 
    Any calls to encoder() will then be intercepted to return the bean instance.
    */
    @Bean
    public BCryptPasswordEncoder encoder() {
        // auto-generate a random salt internally
        return new BCryptPasswordEncoder();
    }
}

