package cs203project.c19.content;

import java.io.*;
import java.nio.file.*;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import cs203project.c19.exception.ContentNotFoundException;

@RestController
public class FileController {
    private FileRepository repository;

    @Autowired
    public FileController(FileRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/content")
    List<File> all() {
        return repository.findAll();
    }

    @PostMapping("/content")
    File newFile(@RequestBody File newFile) {
        return repository.save(newFile);
    }

    @GetMapping("/content/{id}")
    File getFile(@PathVariable Long id) {
        return repository.findById(id)
        .orElseThrow(() -> new ContentNotFoundException(id));
    }

    @PutMapping("/content/{id}")
    File replaceFile(@RequestBody File newFile, @PathVariable Long id) {
        return repository.findById(id)
        .map(file -> {
        file.setCategory(newFile.getCategory());
        file.setTitle(newFile.getTitle());
        file.setContent(newFile.getContent());
        return repository.save(file);
        })
        .orElseGet(() -> {
        newFile.setId(id);
        return repository.save(newFile);
        });
    }

    @DeleteMapping("/content/{id}")
    public void deleteFile(@PathVariable Long id) {
        repository.deleteById(id);
    }

    @RequestMapping("/create_post")
    public ModelAndView viewEmployerPostForm(Model model) {
        model.addAttribute("post", new File());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("employer_post_form");
        return modelAndView;
    }

    @PostMapping("/create_post")
    public ModelAndView savePost(@ModelAttribute(name = "file") File file,
    @RequestParam("image") MultipartFile multipartFile) throws IOException {

        ModelAndView modelAndView = new ModelAndView();

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        file.setContent(fileName);

        File savedfile = repository.save(file);

        String uploadDir = "/file-content/" + savedfile.getId();
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not save upload file:" + fileName);
        }
        
        modelAndView.setViewName("post_success");
        return modelAndView;
    }
} 
