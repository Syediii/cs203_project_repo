package cs203project.c19.content;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
// @NoArgsConstructor
@EqualsAndHashCode
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Date dateCreated = new Date();

    @NotNull(message = "Category should not be null")
    // We define two categories: vaccination or SMM (safe management measures)
    private String category;

    @NotNull(message = "Title should not be null")
    private String title;

    @NotNull(message = "Content should not be null")
    private String content;

    public File(String category, String title, String content) {
        this.category = category;
        this.title = title;
        this.content = content;
    }

    public File() {}

    public String getCategory(File file) {
        return this.category;
    }

    public String getTitle(File file) {
        return this.title;
    }

    public String getContent(File file) {
        return this.content;
    }
}
