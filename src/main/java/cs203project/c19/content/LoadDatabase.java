package cs203project.c19.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    // don't uncomment, will lead to duplicate entries
    // @Bean
    // CommandLineRunner initDatabase(FileRepository repository) {
    //     return args -> {
    //         log.info("Preloading " + repository.save(new File("vaccination", 
    //         "Booster shots", "Walk-in available for ages above 60")));
    //         log.info("Preloading " + repository.save(new File("SMM", "Dining in", 
    //         "Up to 2 vaccinated people")));
    //     };
    // }
}
