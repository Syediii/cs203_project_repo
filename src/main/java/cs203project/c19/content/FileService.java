package cs203project.c19.content;

import java.util.List;

public interface FileService {
    void createFile(File file);
    //public abstract void updateFile(Long id, File file);
    //public abstract void deleteFile(Long id);

    List<File> getFiles();
}
