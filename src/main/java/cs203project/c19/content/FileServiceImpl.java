package cs203project.c19.content;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {
    private FileRepository fileRepo;

    @Autowired
    public FileServiceImpl(FileRepository fileRepo) {
        this.fileRepo = fileRepo;
    }

    private static Map<Long, File> fileRepo1 = new HashMap<>();

    @Override
    public void createFile(File file) {
        fileRepo1.put(file.getId(), file);
    }

    /*@Override
    public void updateFile(Long id, File file) {
        fileRepo.remove(id);
        file.setId(id);
        fileRepo.put(id, file);
    }*/

    /*@Override
    public void deleteFile(Long id) {
        fileRepo.remove(id);
    }*/

    @Override
    public List<File> getFiles() {
        return fileRepo.findAll();
    }

    // @Override
    // public List<CompanyGroup> listCompanyGroups() {
    //     return companyGroups.findAll();
    // }
}
