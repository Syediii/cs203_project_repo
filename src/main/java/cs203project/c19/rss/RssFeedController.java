package cs203project.c19.rss;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;

import cs203project.c19.content.File;
import cs203project.c19.content.FileRepository;

@RestController
public class RssFeedController {
    @Autowired  
    private RssFeedView view;

    private FileRepository fileRepo;
    private Map<String, Object> model;
    @Autowired
    public RssFeedController(FileRepository fileRepo){
        this.fileRepo = fileRepo;
    }
    
    // @GetMapping("/rss")
    // public View getFeed(){
    //     return this.view;
    // }
    @GetMapping("/rss")
    public View getFeed(Model model){
        List<File> files = fileRepo.findAll();
        model.addAttribute("feedContent", files);
        return this.view;
    }

    @GetMapping("/rss/{id}")
    public View getFeedItem(Model model, @PathVariable Long id){
        Optional<File> file = fileRepo.findById(id);
        file.ifPresentOrElse(
            (value)
                -> { model.addAttribute("feedContent", file);},
            ()
                -> { model.addAttribute("feedContent", "");});
            
        return this.view;
    }
    

}
