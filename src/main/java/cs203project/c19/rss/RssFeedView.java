package cs203project.c19.rss;

import java.util.Map;
import java.util.Optional;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Content;
import com.rometools.rome.feed.rss.Item;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.feed.AbstractRssFeedView;

import cs203project.c19.content.File;

@Component
public class RssFeedView extends AbstractRssFeedView{

    @Override
    protected void buildFeedMetadata(Map<String, Object> model,
        Channel feed, HttpServletRequest request){
            feed.setTitle("c19");
            feed.setDescription("Covid 19 Application");
            feed.setLink("http://www.covid19application.com");
            super.buildFeedMetadata(model, feed, request);
        }

    @Override
    protected List <Item> buildFeedItems(Map <String, Object> model,
        HttpServletRequest request, HttpServletResponse response) {
            
            List<Item> items = new ArrayList<Item>();
            Object ob = model.get("feedContent");

            if (ob instanceof List){
                for(int i = 0; i< ((List<?>)ob).size(); i++){
                    Object feedObj = ((List<?>)ob).get(i);
                    File file = (File)feedObj;
                    Item item = new Item();
                    item.setTitle(file.getTitle());
                    item.setPubDate(file.getDateCreated());
                    Content content = new Content();
                    content.setValue(file.getContent());
                    item.setContent(content);
                    items.add(item);
                }
            }
            else if (ob instanceof Optional){
                    Object feedObj = ((Optional<?>)ob).get();
                    File file = (File) feedObj;
                    Item item = new Item();
                    item.setTitle(file.getTitle());
                    item.setPubDate(file.getDateCreated());
                    Content content = new Content();
                    content.setValue(file.getContent());
                    item.setContent(content);
                    items.add(item);
            }
            return items;
    }
}
