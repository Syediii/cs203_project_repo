package cs203project.c19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cs203project.c19.rss.RssFeedController;
import cs203project.c19.user.User;
import cs203project.c19.user.UserRepository;

@SpringBootApplication
public class C19Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(C19Application.class, args);

		// Jpa User respository init
        UserRepository users = ctx.getBean(UserRepository.class);
        BCryptPasswordEncoder encoder = ctx.getBean(BCryptPasswordEncoder.class);
        RssFeedController rss = ctx.getBean(RssFeedController.class);
        // add admin
        // don't uncomment, will lead to duplicate admin accounts and you wont be able to login with admin account
        // System.out.println("[Add user]: " + users.save(
        // new User("admin", encoder.encode("goodpassword"), "ROLE_ADMIN", "admin@gmail.com", "admin_industry", "", "", true, "admin_employment", true)).getUsername());
    }
}
