package cs203project.c19;


import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cs203project.c19.user.CustomUserDetailsService;
import cs203project.c19.user.User;
import cs203project.c19.user.UserRepository;

@SpringBootTest
class C19UserTests {
    @MockBean
    UserRepository userRepo;

    @InjectMocks
    CustomUserDetailsService userService;

    @Test
    void contextLoads() {
    }

    @Test
    public void save_newUser() {
        User user1 = new User("admin", "goodpassword", "ROLE_ADMIN", "admin@gmail.com", "admin_industry", "", "", true,
                "admin_employment", true);

        userRepo.save(user1);
        when(userRepo.save(any(User.class))).thenReturn(user1);
        when(userRepo.findByEmail("admin@gmail.com")).thenReturn(user1);
        verify(userRepo).save(user1);
    }

    @Test
    public void null_unauthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNull(authentication);
    }

    @Test
    public void update_userDetails() {
        User user2 = new User("admin", "goodpassword", "ROLE_ADMIN", "admin@gmail.com", "admin_industry", "", "",
        true, "admin_employment", true);

        when(userRepo.save(any(User.class))).thenReturn(user2);
        
        userService.updateIsVaccinatedTest(user2, false);
        assertEquals(false, user2.getIsVaccinated());
    }

}
