package cs203project.c19.companygroup;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class CompanyGroupServiceTest {
    @Mock
    private CompanyGroupRepository groupRepo;

    @InjectMocks
    private CompanyGroupServiceImpl groupService;

    @Test
    void createGroup_NewGroup_ReturnSuccess() {
        // arrange ***
        CompanyGroup grp = new CompanyGroup("testgroup1", "Restaurant", "admin");
        groupRepo.findCompanyGroupBycompanyGroupName(grp.getCompanyGroupName());
        // mock the "save" operation 
        when(groupRepo.save(any(CompanyGroup.class))).thenReturn(grp);
        // assert ***
        CompanyGroup savedGrp = groupService.addCompanyGroup(grp);

        assertNotNull(savedGrp); //check that the Group was added successfully
        // check that a certain behaviour happened once only
        verify(groupRepo).findCompanyGroupBycompanyGroupName(grp.getCompanyGroupName()); 
        verify(groupRepo).save(grp);
    }

    @Test
    void createGroup_GroupNameAlreadyExists_ReturnFailure() {
        // arrange ***
        CompanyGroup grp = new CompanyGroup("testgroup2", "Restaurant", "admin");
        List<CompanyGroup> sameGroupName = new ArrayList<CompanyGroup>();
        sameGroupName.add(new CompanyGroup("testgroup2", "Restaurant", "admin"));

        groupRepo.findCompanyGroupBycompanyGroupName(grp.getCompanyGroupName());
        // act
        CompanyGroup savedGrp = groupService.addCompanyGroup(grp);
        // assert
        assertNull(savedGrp);
        verify(groupRepo).findCompanyGroupBycompanyGroupName(grp.getCompanyGroupName());
    }

    @Test
    void updateGroupName_NotFound_ReturnNull() {
        CompanyGroup grp = new CompanyGroup("testgroup2", "Restaurant", "admin");
        Long grpId = 10L;
        when(groupRepo.findById(grpId)).thenReturn(Optional.empty());

        CompanyGroup updatedGroup = groupService.updateCompanyGroup(grpId, grp);

        assertNull(updatedGroup);
        verify(groupRepo).findById(grpId);
    }
}
